=== Solamar Testimonials ===
Contributors: Ben Kaplan
Tags: testimonials, credits, reviews 
Requires at least: 3.0
Tested up to: 3.4.2
Stable tag: 1.2.1.2

Solamar Testimonials allows you to easily add testimonials to your website within pages or posts, and in the sidebar.

=== Description ===

Solamar Testimonials allows you to easily add testimonials to your website within pages or posts, and in the sidebar.  The sidebar has a rotating slideshow option.

Major new features in Solamar Testimonials include:

* A control for length and speed of transitions.
* A shift to a more specific naming convention to avoid potential conflicts with other plugins.

=== Installation ===

Upload the Solamar Testimonial plugin to your blog, and activate it.

=== Changelog ===

= 1.2.1.2 =
[*] added esc_attr to field input save function

= 1.2.1 = 
[+] Added support information to help screen 
[*] Updated plugin header information

= 1.2.1 = 
[*] Changed shortcode loop to get_posts() from new WP_Query() to fix issues interfering with page loop. 

= 1.2 = 
[*] Slight adjustment to Testimonial detail fields in edit page

= 1.1 = 
[+] Add controls for length of time and length of transition to widget
[*] Slight adjustments to CSS to make styles more generic

= 1.0 =
[*] Base plugin with custom post type, shortcode, and widget.
